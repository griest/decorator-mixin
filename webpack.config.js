const path = require('path')
const webpack = require('webpack')
const {mergeWith, isArray, map} = require('lodash')

const isProduction = process.env.NODE_ENV === `production`
const isTest = process.env.NODE_ENV === `test`
// dev is default
const isDevelopment = process.env.NODE_ENV === `development` || !(isProduction || isTest)

const rootPath = path.resolve(__dirname)
const sourceFile = path.resolve(rootPath, 'index.js')
const outputPath = path.resolve(rootPath, 'dist')

process.traceDeprecation = isDevelopment

function merge (dest, ...sources) {
  return mergeWith(dest, ...sources, (objValue, srcValue) => {
    if (isArray(objValue)) return objValue.concat(srcValue)
  })
}

const config = {
  entry: sourceFile,

  stats: 'verbose',

  output: {
    filename: `decorator-mixin.js`,
    path: outputPath,
    library: 'decorator-mixin',
    libraryTarget: 'umd',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [
          sourceFile,
        ],
        options: {
          formatter: require('eslint-friendly-formatter'),
        },
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          sourceFile,
        ],
      },
      {
        test: /modernizr\.config\.js$/,
        use: ['modernizr-loader'],
      },
    ],
  },
}

const test = {}

const prod = {
  mode: 'production',

  optimization: {
    minimize: true,
  },
}

if (isProduction) {
  merge(config, prod)
}

module.exports = config
