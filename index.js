import map from 'lodash.map'

export default function mixin (...mixins) {
  const methods = mixins.reduce((acc, mxn) => {
    return {
      ...acc,
      ...mxn,
    }
  }, {})

  return ({kind, elements}) => kind === 'class' ? ({
    kind,
    elements: map(elements, el => {
      const ret = methods[el.key] && el.kind === 'method' && el.descriptor.value ? {
        ...el,
        descriptor: {
          ...el.descriptor,
          value: methods[el.key],
        },
      } : el

      delete methods[el.key]

      return ret
    }).concat(map(Object.getOwnPropertyNames(methods).concat(Object.getOwnPropertySymbols(methods)), key => ({
      kind: 'method',
      key,
      placement: 'prototype',
      descriptor: {
        configurable: true,
        value: methods[key],
      },
    }))),
  }) : ({
    kind,
    elements,
  })
}
