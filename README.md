Decorator Mixin
===============

[![npm](https://img.shields.io/npm/v/decorator-mixin.svg)](https://www.npmjs.com/package/decorator-mixin)
[![pipeline status](https://gitlab.com/griest/decorator-mixin/badges/master/pipeline.svg)](https://gitlab.com/griest/decorator-mixin/commits/master)
[![npm](https://img.shields.io/npm/l/decorator-mixin.svg)](https://gitlab.com/griest/decorator-mixin/LICENSE)

An implementation of mixins using the [new Babel class decorators](https://github.com/tc39/proposal-decorators).

Usage
-----

Pass mixin objects into the mixin function as a decorator on the class. Mixins later in the argument list take priority and will override any methods defined by earlier mixins or the class.

Example
-------

```js
const mixinA = {
  methodA () {
    console.log('mixinA')
  },

  methodB () {
    console.log('mixinA')
  },
}

const mixinB = {
  methodB () {
    console.log('mixinB')
  },

  methodC () {
    console.log('mixinB')
  },
}

@mixin(mixinA, mixinB)
class Klass {
  methodA () {
    console.log('class')
  },

  methodB () {
    console.log('class')
  },

  methodD () {
    console.log('class')
  },
}

const obj = new Klass()

obj.methodA() // 'mixinA'
obj.methodB() // 'mixinB'
obj.methodC() // 'mixinB'
obj.methodD() // 'class'
```

Contributing
------------

Feedback and merge requests are welcome!
